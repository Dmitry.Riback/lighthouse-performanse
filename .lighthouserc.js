module.exports = {
  ci: {
    upload: {
      target: 'lhci',
      serverBaseUrl: 'https://obscure-temple-99168.herokuapp.com',
      token: '52a46ecf-955c-4836-9f08-84b696bddfb7'
    },
    collect: {
      settings: {chromeFlags: '--no-sandbox'},
      url: ['https://www.google.com/'],
    },
    assert : {
      assertions : {
        'categories:performance' : [ "warn", { minScore : 0.5 } ] ,
        'categories:accessibility': [ "error" ,  { minScore : 0.5 } ],
        'categories:seo':[ "error" ,  { minScore : 0.5 } ],
        'categories:best-practices': [ "error" ,  { minScore : 0.5 } ]
      }
    }
  },
};