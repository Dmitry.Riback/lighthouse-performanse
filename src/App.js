import logo from './logo.svg';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import './App.css';

function App() {
  return (
  <BrowserRouter>
    <Switch>
      <Route path="/second-page">
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React 1
            </a>
          </header>
        </div>
      </Route>
      <Route path="/">
        <div className="App">
          <header className="App-header">
            {/*<img src={logo} className="App-logo" alt="logo" />*/}
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React 2
            </a>
          </header>
        </div>
      </Route>
    </Switch>
  </BrowserRouter>
  );
}

export default App;
